#!/usr/bin/ppython

from direct.gui.OnscreenText import OnscreenText

class ScoreBoard():

    def __init__(self):
        self.score = 0
        self.text = OnscreenText(text = '',
                                 pos = (-0.9, 0.9),
                                 scale = 0.1,
                                 fg=(1,0,0,1),
                                 )

    def increment(self):
        self.score += 1
        if self.score==1:
            self.text.setText("1 Point")
        else:
            self.text.setText("%d Points"%self.score)
