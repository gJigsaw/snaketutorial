#!/usr/bin/ppython

from camera import Camera
from region import Region

class Display():

    layout = (
        # name        portion_of_screen         orientation 
        # POV          minx  maxx  miny  maxy    h p r         
        ('ahead',     (0.00, 1.00, 0.00, 1.00), (0,0,0,),  ),
        )

    def __init__(self,game):
        for i in range(len(self.layout)):
            name, portion_of_screen, orientation, = self.layout[i]
            camera = Camera(game, name, orientation)
            region = Region(game, portion_of_screen)
            region.assign_camera(camera)
