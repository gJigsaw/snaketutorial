#!/usr/bin/ppython

from direct.showbase.ShowBase import ShowBase

class Game(ShowBase):

    def __init__(self):
        ShowBase.__init__(self)

if __name__ == "__main__":
    game = Game()
    game.run()
