#!/usr/bin/ppython

from camera import Camera
from region import Region

class Display():

    layout = (
        # name        portion_of_screen         orientation mounted
        # POV          minx  maxx  miny  maxy    h p r         
        ('ahead',     (0.15, 0.85, 0.15, 0.85), (0,0,0,),   True, ),
        ('to_up',     (0.15, 0.85, 0.85, 1.00), (0,90,0),   True, ),
        ('to_left',   (0.00, 0.15, 0.15, 0.85), (90,0,0),   True, ),
        ('to_right',  (0.85, 1.00, 0.15, 0.85), (-90,0,0),  True, ),
        ('to_down',   (0.15, 0.85, 0.00, 0.15), (0,-90,0),  True, ),
        ('from_left', (0.00, 0.15, 0.85, 1.00), (-90,0,0),  False, ),
        ('from_right',(0.85, 1.00, 0.85, 1.00), (90,0,0),   False, ),
        ('from_above',(0.85, 1.00, 0.00, 0.15), (0,-90,0),  False, ),
        ('from_below',(0.00, 0.15, 0.00, 0.15), (0,90,0),   False, ),
        )

    def __init__(self,game):
        for i in range(len(self.layout)):
            name, portion_of_screen, orientation, mounted = self.layout[i]
            camera = Camera(game, name, orientation, mounted)
            region = Region(game, portion_of_screen)
            region.assign_camera(camera)
