#!/usr/bin/ppython

from panda3d import core

class SkyBox():

    def __init__(self,game): 
        self.game = game

        # credit to piratePanda for the inverted_sphere.egg
        # http://www.panda3d.org/forums/memberlist.php?mode=viewprofile&u=4277
        self.model = loader.loadModel("models/inverted_sphere.egg")

        # credit to Benjamin "begla" Glatzel for the skybox texture
        # http://blog.movingblocks.net/2011/08/21/free-high-definition-skybox/
        texture = loader.loadCubeMap('models/skybox/Skybox_#.png')

        # enable automatic generation of texture coordinates
        # https://www.panda3d.org/manual/index.php/Automatic_Texture_Coordinates
        self.model.setTexGen(core.TextureStage.getDefault(),
                             core.TexGenAttrib.MWorldPosition)

        # apply an automatic texture transform as computed from the 
        # relative transform between the world and the sphere
        # https://www.panda3d.org/manual/index.php/Projected_Textures
        self.model.setTexProjector(core.TextureStage.getDefault(),
                                   game.world,
                                   self.model)

        self.model.setTexture(texture)
        self.model.setLightOff()
        self._enlarge()
        self._orient()

        # place model in 'background' to render it first
        self.model.setBin('background', 1) 

        self._utterly_ignore_depth()
        self._darken()

        self.model.reparentTo(game.world)

    def _enlarge(self):
        """ we want a nice, big skybox """
        self.model.setScale(1500)

    def _orient(self):
        """ by default, (0,0,0) looks at the ground of our skymap
        so, rotate the model such that (0,0,0) looks 'forward' instead """
        self.model.setHpr(self.model,(180,-90,0))

    def _utterly_ignore_depth(self):
        """ save our graphics card a bit of work by not bothering to calculate depth """
        self.model.setDepthWrite(False)
        self.model.setDepthTest(False)

    def _write_bam(self):
        """ write this model (and the changes we've made to it) to disk so that
        we can then load it (instead of the egg) quite speedily in the future """
        self.model.writeBamFile("Skybox.bam")
        # load later via: loader.loadModel("Skybox.bam")

    def _darken(self):
        """ shade the world a bit darker """
#        darker = core.Vec4(0.2, 0.2, 0.2, 1)  # absolute black
#        darker = core.Vec4(0.2, 0.2, 0.2, 1)  # very dark
        darker = core.Vec4(0.9, 0.9, 0.9, 1)  # slightly darker
#        darker = core.Vec4(1,1,1,1)           # full brightness
        self.model.setColorScale(darker)



