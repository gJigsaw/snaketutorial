#!/usr/bin/ppython

from panda3d import core

class Snake():

    def __init__(self,game):
        self._game = game                       
        self._prepare_node()

    def _prepare_model(self):
        """ load and configure the snake's model """
        self._model = loader.loadModel('box.egg')
        self._model.reparentTo(self.node)
        self._model.setPos(-0.5,-0.5,-0.5)

    def _prepare_node(self):
        """ setup the snake's root node """
        self.node = self._game.world.attachNewNode("snake")
        self.node.reparentTo(self._game.world)
        self._prepare_model()
        self.node.setTransparency(core.TransparencyAttrib.MAlpha)
        self.node.setColor(0.5,0,1,1) # purple
        self.node.setPos(0,0,0)
