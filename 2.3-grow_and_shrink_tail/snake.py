#!/usr/bin/ppython

from panda3d import core

class Snake():

    key_to_hpr = { 'arrow_left':        (90,0,0),  # Turn Left  (increase H)
                   'arrow_right':       (-90,0,0), # Turn Right (decrease H)
                   'arrow_down':        (0,-90,0), # Pivot Down (decrease P)
                   'arrow_up':          (0,90,0),  # Pivot Up   (increase P)
                 }

    def __init__(self,game):
        self._game = game                       
        self._prepare_node()
        self._length = 5
        self.segments = []
        self._load_control_keys()
        self._speed = 6 # speed in units per second
        delay_between_calls = 1.0/self._speed
        taskMgr.doMethodLater(delay_between_calls,self._manage, "Manage Snake")

    def _load_control_keys(self):
        """ prepare Panda3D to handle turn keys """
        for key,hpr in self.key_to_hpr.items():
            base.accept(key,self._turn,[core.Vec3(hpr)])

    def _prepare_model(self):
        """ load and configure the snake's model """
        self._model = loader.loadModel('box.egg')
        self._model.reparentTo(self.node)
        self._model.setPos(-0.5,-0.5,-0.5)

    def _prepare_node(self):
        """ setup the snake's root node """
        self.node = self._game.world.attachNewNode("snake")
        self.node.reparentTo(self._game.world)
        self._prepare_model()
        self.node.setTransparency(core.TransparencyAttrib.MAlpha)
        self.node.setColor(0.5,0,1,1) # purple
        self.node.setPos(0,0,0)

    def _manage(self,task):
        """ the snake's main loop """
        self._shrink_tail()
        self._move()
        self._grow_tail()
        return task.again

    def _move(self):
        """ move 1 unit forward relative to current HPR heading """
        self._last_position = self.node.getPos()
        self.node.setPos(self.node,(0,1,0))

    def _grow_tail(self):
        segment = self.node.copyTo(self._game.world)
        segment.setPos(self._last_position)
        segment.setColor(0,0,1,1) # blue
        self.segments.insert(0,segment)

    def _shrink_tail(self):
        if len(self.segments) >= self._length:
            tail = self.segments[-1]
            tail.removeNode()
            self.segments.remove(tail)

    def _turn(self,hpr):
        self.node.setHpr(self.node,hpr)



