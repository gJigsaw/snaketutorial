import random

from panda3d import core

class Dot():

    def __init__(self,game):
        self._game = game
        self._model = loader.loadModel('box.egg')
        self.node = game.world.attachNewNode("dot")
        self._model.reparentTo(self.node)
        self._model.setPos(-0.5,-0.5,-0.5)
        self.node.setTransparency(core.TransparencyAttrib.MAlpha)
        self.node.setColor(0,1,0,0.9) # nearly opaque green

