#!/usr/bin/ppython

from panda3d import core

class Snake():

    key_to_hpr = { 'arrow_left':        (90,0,0),  # Turn Left  (increase H)
                   'arrow_right':       (-90,0,0), # Turn Right (decrease H)
                   'arrow_down':        (0,-90,0), # Pivot Down (decrease P)
                   'arrow_up':          (0,90,0),  # Pivot Up   (increase P)
                   'shift-arrow_left':  (0,0,-90), # Roll Left  (increase R)
                   'shift-arrow_right': (0,0,90),  # Roll Right (decrease R)
                 }

    def __init__(self,game):
        self._game = game                       
        self._prepare_node()
        self._length = 5
        self.segments = []
        self.has_moved = False
        self._load_control_keys()
        self._speed = 6 # speed in units per second
        delay_between_calls = 1.0/self._speed
        taskMgr.doMethodLater(delay_between_calls,self._manage, "Manage Snake")

    def _load_control_keys(self):
        """ prepare Panda3D to handle turn keys """
        for key,hpr in self.key_to_hpr.items():
            base.accept(key,self._turn,[core.Vec3(hpr)])

    def _prepare_model(self):
        """ load and configure the snake's model """
        self._model = loader.loadModel('box.egg')
        self._model.reparentTo(self.node)
        self._model.setPos(-0.5,-0.5,-0.5)

    def _prepare_node(self):
        """ setup the snake's root node """
        self.node = self._game.world.attachNewNode("snake")
        self.node.reparentTo(self._game.world)
        self._prepare_model()
        self.node.setTransparency(core.TransparencyAttrib.MAlpha)
        self.node.setColor(0.5,0,1,1) # purple
        self.node.setPos(0,0,0)
#        self.node.setScale(0.9)

    def _ate_dot(self):
        """ return true if the snake has collided with the dot """
        return self._game.collision(self.node,self._game.dot.node)

    def _manage(self,task):
        """ the snake's main loop """
        self._shrink_tail()
        self._move()
        if self._ate_dot():
            self._length += 5
            self._game.score.increment()
            self._game.dot.eaten()
        elif self._ran_into_self():
            self.node.setPos(self._last_position)
            self.has_moved = False
            self._game.end_game()
            return task.done            
        self._grow_tail()
        return task.again

    def _move(self):
        """ move 1 unit forward relative to current HPR heading """
        vector = self.node.getRelativeVector(self.node,(0,1,0))
        self._last_position = self.node.getPos()
        self.node.setPos(self.node,vector)
        self.has_moved = True

    def _ran_into_self(self):
        """ return true if snake has collided with any segment """
        return any([self._game.collision(S,self.node) for S in self.segments])

    def _grow_tail(self):
        segment = self.node.copyTo(self._game.world)
        segment.setPos(self._last_position)
        segment.setColor(0,0,1,1) # blue
        self.segments.insert(0,segment)

    def _shrink_tail(self):
        if len(self.segments) >= self._length:
            tail = self.segments[-1]
            tail.removeNode()
            self.segments.remove(tail)

    def _turn(self,hpr):
        if self.has_moved:
            self.node.setHpr(self.node,hpr)
        self.has_moved = False



