#!/usr/bin/ppython

from direct.gui.OnscreenText import OnscreenText

class Helper():
    """ on screen text with helpful debug data """

    def __init__(self,game,region):
        self.game = game
        self.region = region
        self.camera = region.camera
        self.text = OnscreenText(text = 'foo',
                                 pos = self._position(),
                                 scale = self._scale(),
                                 fg = (0,0,1,1),
                                 )


    def _position(self):
        """ center the text within the region """
        x,y = self.region.center
        xs,ys = self.region.x_scale, self.region.y_scale
        x = x * 2 - 1
        y = y * 2 - 1
        return x,y

    def _scale(self):
        """ scale down the text to 15% of the region size """
        return (self.region.x_scale + self.region.y_scale)/2 *.25

    def manage(self,task):

        text = ''
#        text += self.camera.name+'\n'

        shown = {
#            'ori': self.camera.orientation,
#            'V': self.camera._vector(),
#            'ctr': self.region.center,
#            'rxs': self.region.x_scale,
#            'rys': self.region.y_scale,
#            'hp': self._position(),
#            'pos':self.camera.camera.getPos(),
#            'p':self.camera.position,
#            'dpos':self.game.dot.node.getPos(),
#            'spos':self.game.snake.node.getPos(),
#            'shpr':self.game.snake.node.getHpr(),
#            'hpr':self.camera.camera.getHpr(),
#            'od':round(self.camera._optimal_distance()),
#            'op':self.camera._optimal_position(),
#            's2d':round(self.camera._snake_to_dot()),
#            'dpd':self.camera._distance_past_dot(),
            }

        for k,v in shown.items():
            text+="%s:%s\n"%(str(k),str(v))

        self.text.setText(text)
        return task.again

