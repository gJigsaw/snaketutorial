class Region():
    """ a portion of the display window"""

    def __init__(self,game,portion_of_screen):
        p = portion_of_screen
        self.region = game.win.makeDisplayRegion(*p)
        self.center = ((p[0]+p[1])/2, (p[2]+p[3])/2)
        self.x_scale = (p[1]-p[0])
        self.y_scale = (p[3]-p[2])

    def assign_camera(self,camera):
        self.camera = camera
        self.region.setCamera(camera.camera)

