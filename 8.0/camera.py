from panda3d import core

class Camera():
    """ binds a view of the scene graph to a display region """

    def __init__(self,game,name,orientation,mounted):
        self._game = game
        self.name = name
        self.node = self._game.world.attachNewNode("camera %s"%self.name)
        self.camera = self._game.makeCamera(self._game.win)
        camera_node = self.camera.node()
        self.camera.reparentTo(self.node)
        self.node.setHpr(orientation)
        self.node.reparentTo(self._game.snake.node)
        camera_node.getDisplayRegion(0).setActive(0)    # FIXME - Why do we need this?
        camera_node.getLens().setNear(0.49) # down from default of 1.0
        if mounted:
            camera_node.getLens().setFov(90) # up fro mthe default of 40
            if self.name == 'ahead':
#                camera_node.getLens().setFov(110)
                pass
        else:
            taskMgr.add(self._manage, "Manage %s Camera"%self.name)

    def _vector(self):
        """ a straight line in the direction the camera faces """
        x,y,z = self._game.world.getRelativeVector(self.node,(0,1,0))
        x,y,z = (int(round(i)) for i in (x,y,z))
        return (x,y,z)

    def _planar_distance(self,a,b):
        """returns the distance between two points on the
        plane perpendicular to the camera's vector"""
        x,y,z = self._vector()
        if int(y):
            return (a.getXz() - b.getXz()).length()
        elif int(x):
            return (a.getYz() - b.getYz()).length()
        elif int(z):
            return (a.getXy() - b.getXy()).length()

    def _snake_to_dot(self):
        """return planar distance between snake and dot"""
        snake = self._game.snake.node.getPos()
        dot = self._game.dot.node.getPos()
        return self._planar_distance(snake,dot)

    def _optimal_distance(self):
        """returns the optimal camera distance

        tries to keep both the snake and the dot inside the field of view
        """
        minimum_distance = 30
        maximum_distance = 150
        d = self._snake_to_dot() * 4
        d = max(minimum_distance,d)
        d = min(maximum_distance,d)
        return d

    def _optimal_position(self):
        """returns the optimal camera position"""
        d = self._optimal_distance()
        x,y,z = self.node.getRelativeVector(self._game.snake.node,(0,1,0))
        return (x*d,y*d,z*d)

    def _manage(self,task):
        """ zoom in and out, trying to keep the dot in frame """
        self.node.setPos(self._optimal_position())
        return task.again

