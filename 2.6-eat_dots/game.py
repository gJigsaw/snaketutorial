#!/usr/bin/ppython

from direct.showbase.ShowBase import ShowBase

from snake import Snake
from dot import Dot
from skybox import SkyBox

class Game(ShowBase):

    def __init__(self,limits):
        self.limits = limits
        self._setup_screen()
        self.world = render.attachNewNode("world")
        self.snake = Snake(self)
        self.skybox = SkyBox(self)
        self.dot = Dot(self)

    def _setup_screen(self):
        ShowBase.__init__(self)
        self._setup_camera()

    def _setup_camera(self):
        orientation = (0,-90,0)
        camera = base.cam
        camera.setHpr(orientation)
        camera.setPos(0,0,30)
        camera_node = camera.node()
        camera_node.getLens().setNear(0.49) # down from default of 1.0
        camera_node.getLens().setFov(90) # up from the default of 40

    def collision(self,a,b):
        """ return True if a and b have collided """
        return a.getDistance(b) < .9

if __name__ == "__main__":
    limits = (20,20,1)
    game = Game(limits)
    game.run()
