import random

from panda3d import core

class Dot():

    def __init__(self,game):
        self._game = game
        self._limits = game.limits
        self._model = loader.loadModel('box.egg')
        self.node = game.world.attachNewNode("dot")
        self._model.reparentTo(self.node)
        self._model.setPos(-0.5,-0.5,-0.5)
        self.node.setTransparency(core.TransparencyAttrib.MAlpha)
        self.node.setColor(0,1,0,0.9) # green
        self._relocate_randomly()

    def _get_random_point(self):
        """ return a random position within the game limits """
        return core.Point3(random.choice(range(self._limits[0])),
                           random.choice(range(self._limits[1])),
                           random.choice(range(self._limits[2])))

    def _relocate_randomly(self):
        """ move to a random position within the game limits """
        self.node.setPos(self._get_random_point())

    def eaten(self):
        """ move elsewhere """
        self._relocate_randomly()

