#!/usr/bin/ppython

from panda3d import core

class Snake():

    key_to_hpr = { 'arrow_left':        (90,0,0),  # Turn Left  (increase H)
                   'arrow_right':       (-90,0,0), # Turn Right (decrease H)
                   'arrow_down':        (0,-90,0), # Pivot Down (decrease P)
                   'arrow_up':          (0,90,0),  # Pivot Up   (increase P)
                 }

    def __init__(self,game):
        self._game = game                       
        self._prepare_node()
        self._load_control_keys()

    def _load_control_keys(self):
        """ prepare Panda3D to handle turn keys """
        for key,hpr in self.key_to_hpr.items():
            base.accept(key,self._turn,[core.Vec3(hpr)])

    def _prepare_model(self):
        """ load and configure the snake's model """
        self._model = loader.loadModel('box.egg')
        self._model.reparentTo(self.node)
        self._model.setPos(-0.5,-0.5,-0.5)

    def _prepare_node(self):
        """ setup the snake's root node """
        self.node = self._game.world.attachNewNode("snake")
        self.node.reparentTo(self._game.world)
        self._prepare_model()
        self.node.setTransparency(core.TransparencyAttrib.MAlpha)
        self.node.setColor(0.5,0,1,1) # purple
        self.node.setPos(0,0,0)

    def _turn(self,hpr):
        self.node.setHpr(self.node,hpr)



