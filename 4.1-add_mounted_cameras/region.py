class Region():
    """ a portion of the display window"""

    def __init__(self,game,portion_of_screen):
        p = portion_of_screen
        self.region = game.win.makeDisplayRegion(*p)

    def assign_camera(self,camera):
        self.camera = camera
        self.region.setCamera(camera.camera)

