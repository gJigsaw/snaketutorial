from panda3d import core

class Camera():
    """ binds a view of the scene graph to a display region """

    def __init__(self,game,name,orientation,mounted):
        self._game = game
        self.name = name
        self.node = self._game.world.attachNewNode("camera %s"%self.name)
        self.camera = self._game.makeCamera(self._game.win)
        camera_node = self.camera.node()
        self.camera.reparentTo(self.node)
        self.node.setHpr(orientation)
        self.node.reparentTo(self._game.snake.node)
        camera_node.getDisplayRegion(0).setActive(0)    # FIXME - Why do we need this?
        camera_node.getLens().setNear(0.49) # down from default of 1.0
        if mounted:
            camera_node.getLens().setFov(90) # up from the default of 40
        else:
            self.node.setPos(self._optimal_position())

    def _optimal_position(self):
        """returns the optimal camera position"""
        d = self._optimal_distance()
        x,y,z = self.node.getRelativeVector(self._game.snake.node,(0,1,0))
        return (x*d,y*d,z*d)

    def _optimal_distance(self):
        """ returns the optimal camera distance """
        return 30


