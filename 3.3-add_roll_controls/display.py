#!/usr/bin/ppython

class Display():

    def __init__(self,game):
        orientation = (0,0,0)
        self.node = game.world.attachNewNode("camera")
        camera = game.makeCamera(game.win)
        camera_node = camera.node()
        camera.reparentTo(self.node)
        self.node.setHpr(orientation)
        self.node.reparentTo(game.snake.node)
        camera_node.getDisplayRegion(0).setActive(0)    # FIXME - Why do we need this?
        camera_node.getLens().setNear(0.49) # down from default of 1.0
        camera_node.getLens().setFov(90) # up from the default of 40
        portion_of_screen = (0.00, 1.00, 0.00, 1.00)
        region = game.win.makeDisplayRegion(*portion_of_screen)
        region.setCamera(camera)

