#!/usr/bin/ppython

from direct.showbase.ShowBase import ShowBase

from dot import Dot

class Game(ShowBase):

    def __init__(self):
        self._setup_screen()
        self.world = render.attachNewNode("world")
        self.dot = Dot(self)

    def _setup_screen(self):
        ShowBase.__init__(self)
        self._setup_camera()

    def _setup_camera(self):
        orientation = (0,-90,0) # HPR, Heading, Pivot, Roll
        camera = base.cam       # Panda3D likes __builtins__ !
        camera.setHpr(orientation)
        camera.setPos(0,0,3)    # X = right, Y = into, Z = Up
        camera_node = camera.node()
        camera_node.getLens().setNear(0.49) # down from default of 1.0
        camera_node.getLens().setFov(90)    # up from the default of 40

if __name__ == "__main__":
    game = Game()
    game.run()
