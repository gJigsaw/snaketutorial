#!/usr/bin/ppython

from direct.showbase.ShowBase import ShowBase

from dot import Dot

class Game(ShowBase):

    def __init__(self):
        self._setup_screen()
        self.world = render.attachNewNode("world")
        self.dot = Dot(self)

    def _setup_screen(self):
        ShowBase.__init__(self)

if __name__ == "__main__":
    game = Game()
    game.run()
